#+TITLE: Catastrophe at Versailles
#+DATE: <2020-01-21 Tue>
#+AUTHOR: Soren
#+EMAIL: soren@disroot.org
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 26.3 (Org mode 9.1.9)

** Catastrophe at Versailles

   Wilson argued that no victor should be declared after World War
   I. While Britain and France weren't supportive of this idea, their
   people were. Wilson's ideas eventually formed into the League of
   Nations. The league of Nations was a basic predecessor to the
   United Nations and NATO.

*** The Fate of Wilson's Ideas

    Of the peace conference at Versailles included 10,000
    representatives from around the globe. However, the attendees were
    mainly American and French and British leaders. When the Japanese
    delegation proposed a declaration for equal treatment of all
    races, the Allies rejected it. Similarly, the Allies ignored a
    global pan- African Congress organized by W. E. B. Du Bois and
    other black leaders. Additionally, the Allies disrespected Arab
    representatives who had been military allies during the war, and
    even Italy's Prime Minister. The allies excluded Russia and
    Germany as well because they did not trust Russia's communist
    leaders and wanted to dictate terms for Germany.

    Both Britain and France blamed Germany for the war. Without others
    knowing, they divided and claimed Germany's African colonies. They
    forced Germany to pay $33 billion in reparations, which caused
    resentment and economic hardship in Germany, helping lead to World
    War II.

    As part of this, Britain planned to establish a "national home for
    the Jewish people" in Palestine. Because of this, Jewish people
    began moving to Palace dean, purchasing land, and in some cases
    evicting Palestinian tenants. As early as 1920, riots erupted
    between the Jewish and Palestinians, a situation that would
    escalate beyond British control.

    In a way, the treaty of Versailles created conditions that would
    lead only to future bloodshed, and is widely regarded as one of
    history's great catastrophes.

    #+BEGIN_QUOTE
    "Those three all-powerful, all-ignorant men\dots{}sitting there
    carving continents with only a child to lead them." -- Arthur
    Balfour
    #+END_QUOTE

*** Congress Rejects the Treaty

    At the time when the Treaty of Versailles needed to be accepted by
    Congress,hostile Republicans held a majority in the Senate. Many
    worried that Article X, the provision for collective security,
    would prevent the United States from pursuing an independent
    foreign policy. They worried that the United States would be
    controlled by the other nations in the League.

    Wilson wanted to get as much support for the tree as possible, so
    he toward the country giving speeches. He exhausted himself and in
    September 1919, he collapsed a week later, he suffered a stroke
    that left one side of his body paralyzed, and even though he still
    urged Democratic Senators to reject Republican amendments, the
    treaty failed to win a two thirds majority, even a 2nd attempt in
    March 1920 fell 7 votes short.

    The treaty, and Wilson's leadership abilities, were essentially
    null. On the outside, the war seemed to usher in an era of
    progress. On the inside, however, any positive change was being
    undone.
