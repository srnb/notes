#!/bin/bash

outdir=public

declare -a subjects=(
  "AP Calculus BC"
  # "English 10 Pre-AP"
  # "Physics Pre-AP"
  "AP US History"
  "AP Psychology"
)

date=`date +"%Y-%m-%d %a"`

# ====================== #

rm -rf "$outdir"
find . -name "index.org" -exec bash -c 'rm "$0"' {} \;
mkdir -p "$outdir"

read -r -d '' main << EOM
#+OPTIONS: org-html-tableel-org:t
#+TITLE: Notes
#+DATE: $date
#+AUTHOR: Soren
#+EMAIL: soren@disroot.org
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+HTML_HEAD: <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/sorenbug/orgcss/org.css" />
#+HTML_HEAD: <script src="https://cdn.jsdelivr.net/gh/sorenbug/orgcss/highlight.pack.js" type="text/javascript"></script>
#+HTML_HEAD: <script type="text/javascript">hljs.initHighlightingOnLoad();</script>

EOM
for subject in "${subjects[@]}"; do
  read -r -d '' header << EOM
#+OPTIONS: org-html-tableel-org:t
#+TITLE: $subject
#+DATE: $date
#+AUTHOR: Soren
#+EMAIL: soren@disroot.org
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+HTML_HEAD: <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/sorenbug/orgcss/org.css" />
#+HTML_HEAD: <script src="https://cdn.jsdelivr.net/gh/sorenbug/orgcss/highlight.pack.js" type="text/javascript"></script>
#+HTML_HEAD: <script type="text/javascript">hljs.initHighlightingOnLoad();</script>

EOM
  main="$main"$'\n'" - [[file:$subject/index.org][$subject]]"
  content=""
  for filename in $(ls "$subject"); do
    fl=$(grep -n -m 1 "^*" "$subject/$filename" | sed  's/\([0-9]*\).*/\1/')
    title=$(head -n "$fl" "$subject/$filename" | tail -n 1)
    level=$(echo "$title" | grep -o "*" | wc -l)
    content="$content"$'\n\n'"#+INCLUDE: \"./$filename\" :lines \""$fl"-\" :minlevel $level"
  done
  tc="$header$content"
  echo "$tc" > "$subject/index.org"
done
echo "$main" > "index.org"

while read orgfile; do
  emacs "$orgfile" --batch -l "$(pwd)/orgtableformat.el" -f org-html-export-to-html --kill
  filename=$(basename -- "$orgfile")
  filename="${filename%.*}"
  nf="$filename.html"
  d=$(dirname "$orgfile")
  mkdir -p "public/$d"
  mv "$d/$nf" "public/$d"
done < <( find . -name "*.org" )

# find . -name "index.org" -exec bash -c 'mv "$0" "./$(outdir)/$0"' {} \;
