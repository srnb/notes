#+TITLE: The Turning Point: 1863
#+DATE: <2019-11-20 Wed>
#+AUTHOR: Soren
#+EMAIL: soren@disroot.org
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 26.3 (Org mode 9.1.9)

** The Turning Point: 1863

   TODO

*** Emancipation

    TODO

**** "Contrabands"

     TODO

**** The Emancipation Proclamation

     TODO

*** Vicksburg and Gettysburg

    TODO

**** The Battle for the Mississippi

     TODO

**** Lee's Advance and Defeat

     TODO

** The Union Victorious

   TODO

*** Soldiers and Strategy

    TODO

**** The Impact of Black Troops

     TODO

**** Capable Generals Take Command

     TODO

**** Stalemate

     TODO

*** The Election of 1864 and Sherman's March

    TODO

**** The National Union Party Versus the Peace Democrats

     TODO

**** The Fall of Atlanta and Lincoln's Victory

     TODO

**** William Tecumseh Sherman: "Hard War" Warrior

     TODO

**** The Confederate Collapse

     TODO
