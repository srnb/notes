#+TITLE: Theorems
#+DATE: <2018-08-29 Wed>
#+AUTHOR: Soren
#+EMAIL: soren@disroot.org
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 25.2.2 (Org mode 9.1.14)

* Theorems
Important theorems for Calculus.

** Theorem Rules
Basic theorems follow a format of "If $x$, then $y$".

*** Converse
Theorems have a converse if they follow "If and only if". "If
and only if $x$, then $y$" becomes "If and only if $y$, then
$x$."

*** Negation
Theorems have a negative if they follow "If and only if". "If
and only if $x$, then $y$" becomes "If and only if not $x$, then
not $y$."

*** Contrapositive
All theorems can have a contrapositive. The contropositive is a
combination of the converse and the negation. "If $x$, then $y$"
becomes "If not $y$, then not $x$".

Contrapositives change logic of $x$ and $y$. Optionals
become required, "And" becomes "Or", and vice versa.

** Intermediate Value Theorem
The Intermediate Value Theorem states that if $f$ is continuous
on a closed interval $[a, b]$, a $c$ exists for any $k$ where
$k$ is between $f(a)$ and $f(b)$ and $f(c) = k$.

If the $f$ is not continuous on the closed interval, then there
can be a $k$ where there is no $c$ such that $f(c) = k$.

*** Conditional statement
 - If :: there is at least one number $c$ in $[a, b]$ such that
   $f(c) = k$
 - Then :: $f$ is continuous on the closed interval $[a, b]$,
   $f(a) \neq f(b)$ or $k$ is any number between $f(a)$ and
   $f(b)$.

*** Usage: Finding x-intercepts of functions
Given a function $f$, if $f(a)$ is positive and $f(b)$
is negative, there is some $c$ where $f(c) = 0$.
 - $f(x) = x^{3} + x - 1$ :: Here, $f(-1) = -1$ and $f(1) = 1$,
   so given an interval $[-1, 1]$ there must be a $c$ such that
   $f(c) = 0$. Solving for $c$, set the equation to $0$.
 - $f(x) = x^{3} + 5x - 3$ :: Here, $f(0) = -3$ and $f(1) = 3$.
   By the IVT, there is a $0 < c < 1$ such that $f(c) = 0$.
 - $g(t) = 2\cos{t} - 3t$ :: Here, $g(0) = 2$ and
   $g\left(\frac{\pi}{2}\right) = -\frac{3\pi}{2}$, so there
   is at least one $c$ where $0 < c < \frac{\pi}{2}$ and
   $g(c) = 0$.
 - $h(\theta) = 1 + \theta - 3\tan{\theta}$ :: Here, $h(0) = 1$
   and $h\left(\frac{\pi}{4}\right) \approx -\frac{5}{4}$. This
   means that there is a $c$ in $\left[0, \frac{\pi}{4}\right]$
   where $h(c) = 0$.

*** Usage: Finding a value inside a set interval
Given a function $f$, a closed interval $[a, b]$, and a
$k$, prove that there is a $c$ such that $f(c) = k$ and
$a < c < b$.
 - $f(x) = x^{2} + x - 1$, $[0, 5]$, $f(c) = 11$ :: Substituting
   $c$ and setting equal to $11$:
   \begin{align}
     & c^{2} + c - 1 &= 11 & \\
     & c^{2} + c - 12 &= 0 & \\
     & c &= \frac{-1 \pm \sqrt{1^{2} - 4(1)(-12)}}{2} & \\
     & c &= \frac{-1 \pm \sqrt{49}}{2} & \\
     & c &= \frac{-1 \pm 7}{2} & \\
     c &= \frac{-8}{2} & c &= \frac{6}{2} \\
     c &= -4 & c &= 3
   \end{align}
   $-4$ is not in the domain, so the solution is $c = 3$.
 - $f(x) = x^{2} - 6x + 8$, $[0, 3]$, $f(c) = 0$ :: Again, substitute
   $c$ and set equal to $0$.
   \begin{align}
     & c^{2} - 6c + 8 &= 0 & \\
     & c &= \frac{6 \pm \sqrt{(-6)^{2} - 4(1)(8)}}{2} & \\
     & c &= \frac{6 \pm \sqrt{36 - 32}}{2} & \\
     & c &= \frac{6 \pm \sqrt{4}}{2} & \\
     & c &= \frac{6 \pm 2}{2} & \\
     c &= \frac{4}{2} & c &= \frac{8}{2} \\
     c &= 2 & c &= 4
   \end{align}
   Since $4$ is not in $[0, 3]$, the solution is $c = 2$.
 - $f(x) = x^{3} - x^{2} + x - 2$, $[0, 3]$, $f(c) = 4$ :: Again,
   substitute $c$ and set equal to $f(c)$.
   \begin{align}
     c^{3} - c^{2} + c - 2 &= 4 \\
     c^{3} - c^{2} + c - 6 &= 0 \\
     (c - 2)\left(c^{2} + x + 3\right) &= 0 \\
     c &= 2
   \end{align}
   The left over quadratic is not worth solving for solutions
   as due to the cubic not being factorable further said
   solutions would not be real. The result is $c = 2$.
 - $f(x) = \frac{x^{2} + x}{x - 1}$, $\left[\frac{5}{2}, 4\right]$, $f(c) = 6$ :: The
   rational has a nonremovable discontinuity at $x = 1$, but
   $x = 1$ is not included in our domain, so the function is
   continuous over the domain. Guessing integers inside of
   the domain leads to $c = 3$.
   /Author note: I've forgotten how to solve rationals./
