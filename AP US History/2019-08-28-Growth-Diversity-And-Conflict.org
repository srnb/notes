#+TITLE: Growth, Diversity, and Conflict
#+DATE: <2019-09-01 Sun>
#+AUTHOR: Soren
#+EMAIL: soren@disroot.org
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 26.1 (Org mode 9.1.9)

* Growth, Diversity, and Conflict

  People started moving to the restoration colonies like the Carolinas
  to escape the poverty of their home countries. Cultural movements
  like the Enlightenment were furthering this as well.

** New England's Freehold Society

   In the 1630s, when the Puritans left England, the nobles and elites
   held 75% of the land. The others, tenants and property-less
   farmers, worked on the land. However, since the population had
   significantly grown since then, by 1750 this "freehod" ideal was
   threatened.

*** Farm Families: Women in the Household Economy

    The Puritans placed the husband at the head of the house, and very
    rarely gave women any semblance of equality to men. From an early
    age, girls saw how their mothers were bound by a web of legal and
    cultural restrictions.

*** Farm Property: Inheritance

    Marriage in this time allowed the husband to gain full control
    over all of his wife's property and possesions.

*** Freehold Society in Crisis

    The increasing population, doubling each generation, meant that
    families were no longer able to provide adequate support for every
    child under the freehold system. Soon, children started working in
    artisan crafts instead of just farming.

** Diversity in the Middle Colonies

   The Middle Colonies were New York, New Jersey, and Pennsylvania.

*** Economic Growth, Opportunity, and Conflict

    TODO

**** Conflict in the Quaker Colonies

     TODO

*** Cultural Diversity

    TODO

**** The German Influx

     TODO

**** Scots-Irish Settlers

     TODO

*** Religion and Politics

    TODO
