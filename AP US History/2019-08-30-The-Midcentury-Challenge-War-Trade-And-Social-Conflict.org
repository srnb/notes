#+TITLE: The Midcentury Challenge: War, Trade, and Social Conflict
#+DATE: <2019-09-01 Sun>
#+AUTHOR: Soren
#+EMAIL: soren@disroot.org
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 26.1 (Org mode 9.1.9)

** The Midcentury Challenge: War, Trade, and Social Conflict

   TODO

*** The French and Indian War

    TODO

**** Conflict in the Ohio Valley

     TODO

**** The Albany Congress

     TODO

**** The War Hawks Win

     TODO

*** The Great War for Empire

    TODO

*** British Industrial Growth and the Consumer Revolution

    TODO

*** The Struggle for Land in the East

    TODO

*** Western Rebels and Regulators

    TODO

**** The South Carolina Regulators

     TODO

**** Civil Strife in North Carolina

     TODO
