#+TITLE: Class, Culture, and the Second Party System
#+DATE: <2019-10-17 Thu>
#+AUTHOR: Soren
#+EMAIL: soren@disroot.org
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 26.3 (Org mode 9.1.9)

** Class, Culture, and the Second Party System

   TODO

*** The Whig Worldview

    TODO

**** Calhoun's Dissent

     TODO

**** Anti-Masons Become Whigs

     TODO

*** Labor Politics and the Depression of 1837--1843

    TODO

*** "Tipecanoe and Tyler Too!"

    TODO

**** The Log Cabin Campaign

     TODO

**** Tyler Subverts the Whig Agenda

     TODO
